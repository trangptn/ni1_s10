const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require("../index");
const should = chai.should();

chai.use(chaiHttp);

const mongoose = require('mongoose');
const reviewModel = require('../app/models/review.model');

describe("Test MongoDb connect", () => {
    it("Should connect to MongoDb", (done) => {
        chai.request(server)
            .get("/test/mongo-connection")
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property("message").eql("MongoDb connected successfully");
                done();
            })
    })
})

describe("Test Post review", () => {
    it("Result API post Success", (done) => {
        const newReview = {
            "stars": 5,
            "note": "Good",
            "courseId": "65707d6dbb67f496b816d714"
        }
        chai.request(server)
            .post("/api/v1/reviews/")
            .send(newReview)
            .end((err, res) => {
                res.should.have.status(201);
                res.body.should.be.a('object');
                res.body.data.should.have.property("stars").eql(newReview.stars);
                res.body.data.should.have.property('note').eql(newReview.note);
                done();
            })

    })
})
describe("Test CRUD Review Restful API", () => {
    describe("Test get all review", () => {
        it("Result API get all must be array", (done) => {
            chai.request(server)
                .get("/api/v1/reviews/")
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.data.should.be.a('array');
                    done();
                })
        })
    })
})

describe("Test Get Review By Id", () => {
    it("Should get review By valid Id", async () => {
        let review = await reviewModel.create({
            "stars": 7,
            "note": "well",
            "courseId": "661a2664ca07859777f3136f"
        })
        chai.request(server)
            .get(`/api/v1/reviews/${review._id}`)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a("object");
                res.body.should.have.property("data");
            })
    })
})

describe("Test Update Review By Id", () => {
    it("Should update Review By Id Success", async () => {
        let review = await reviewModel.create({
            "stars": "3",
            "note": "Bad",
            "courseId": "661a2664ca07859777f3136f"
        })
        const res = await chai.request(server)
            .put(`/api/v1/reviews/${review._id}`)
            .send({
                stars: 6,
                note: "Good"
            })
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("data");
    })
})
describe("Test Delete Review By Id", () => {
    it("Should delete Review By Id success", async () => {
        let review = await reviewModel.create({
            "stars": 9,
            "note": "very good ",
            "courseId": "661a2664ca07859777f3136f"
        });
        const res = await chai.request(server)
            .delete(`/api/v1/reviews/${review._id}`)
        res.should.have.status(200);
        res.body.should.be.a('object');
    })
})