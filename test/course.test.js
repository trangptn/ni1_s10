const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require("../index");
const should = chai.should();

chai.use(chaiHttp);

const mongoose = require('mongoose');
const courseModel = require('../app/models/course.model');

describe("Test MongoDb connect", () => {
    it("Should connect to MongoDb", (done) => {
        chai.request(server)
            .get("/test/mongo-connection")
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property("message").eql("MongoDb connected successfully");
                done();
            })
    })
})

describe("Test Post course", () => {
    it("Result API post Success", (done) => {
        const newCourse = {
            "reqTitle": "nodejs",
            "reqDescription": "Rest API by Node Js",
            "reqStudent": 15
        }
        chai.request(server)
            .post("/api/v1/courses/")
            .send(newCourse)
            .end((err, res) => {
                res.should.have.status(201);
                res.body.should.be.a('object');
                res.body.data.should.have.property("title").eql(newCourse.reqTitle);
                res.body.data.should.have.property('description').eql(newCourse.reqDescription);
                done();
            })

    })
})
describe("Test CRUD Course Restful API", () => {
    describe("Test get all course", () => {
        it("Result API get all must be array", (done) => {
            chai.request(server)
                .get("/api/v1/courses/")
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.data.should.be.a('array');
                    done();
                })
        })
    })
})

describe("Test Get Course By Id", () => {
    it("Should get course By valid Id", async () => {
        let course = await courseModel.create({
            "title": "Ten",
            "description": "Mo ta",
            "noStudent": 10
        })
        chai.request(server)
            .get(`/api/v1/courses/${course._id}`)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a("object");
                res.body.should.have.property("data");
            })
    })
})

describe("Test Update Course By Id", () => {
    it("Should update Course By Id Success", async () => {
        let course = await courseModel.create({
            "title": "Ten 1",
            "description": "Mo ta",
            "noStudent": 10
        })
        const res = await chai.request(server)
            .put(`/api/v1/courses/${course._id}`)
            .send({
                reqDescription: "Mo ta 1",
                reqStudent: 11
            })
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("data");
    })
})
describe("Test Delete Course By Id", () => {
    it("Should delete Course By Id success", async () => {
        let course = await courseModel.create({
            "title": "Ten 2",
            "description": "Mo ta ",
            "noStudent": 10
        });
        const res = await chai.request(server)
            .delete(`/api/v1/courses/${course._id}`)
        res.should.have.status(200);
        res.body.should.be.a('object');
    })
})